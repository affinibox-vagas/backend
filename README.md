## Apresentação

A Affinibox é uma plataforma de gestão de Clubes de Parcerias.
Oferecemos uma solução whitelabel para associações e empresas que queiram disponibilizar suas parcerias a seus colaboradores de forma mensurável e prática. 
Parte do nosso trabalho é prospectar novas parcerias e temos alguns grandes nomes.
A composição da empresa é bastante enxuta e a gestão é bastante horizontalizada, assim, as decisões são bastante rápidas.
Todos no time tem influência em como o produto se desenvolve e como os problemas dos nossos clientes são resolvidos.


## Descrição da vaga

Procuramos um Dev. PHP Backend Pleno/Sênior para trabalhar na produção, documentação e testes de nossa API.

Além disso, você atuará também na infraestrutura e em todas as decisões que envolvam o Backend da plataforma.

## Local

Escritório, São Paulo - Entre a Oscar Freire e Haddock Lobo, relativamente perto da Paulista, muito próxima a estação Oscar Freire.

## Benefícios

Plano de saúde (Omint)

Vale Refeição (~30/dia)

Vale Alimentação (~450/mês)

Vale Transporte

Horário flexível

## Requisitos

* **Obrigatórios:**
    * Criação de APIs
    * Patterns OOP
    * BD Relacional (MySQL)
    * Framework Laravel (5.5+), todo seu ecossistema, patterns e novidades
    * AWS (utilizamos o kit padaria)
    * Algolia / CloudSearch / ElasticSearch
    * Organização e documentação de código
    * Testes
    * Boa vontade em ensinar e aprender com as outras pessoas do time

* **Desejáveis:**
    * Docker
    * Google Analytics
    * Ensino Superior Completo
    * Conhecimentos em CI/CD
    * Wordpress (fique tranquilo, utilizamos somente em algumas landing pages)

## Contratação

CLT (40h), salário a combinar.


## Como se candidatar

Envie um e-mail para `emanuel.meli@affinibox.com.br` com o assunto [Vaga Backend] {SEU_NOME}

O e-mail deve conter:
* Breve apresentação
* CV ou Linkedin (preferimos Linkedin!)
* GitHub